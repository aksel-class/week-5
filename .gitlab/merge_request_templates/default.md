## Description
Brief description of the feature and its purpose.

## Type of Feature
Specify the type of feature:
- [ ] New functionality
- [ ] UI/UX enhancement
- [ ] Performance improvement
- [ ] Bug fix
- [ ] Refactoring
- [ ] Documentation update
- [ ] Other (please specify)

## Additional Notes
Any additional information or context that might be helpful.

CC: @resky.pv

Thank you for your time and effort in reviewing this merge request. Your feedback and suggestions are highly appreciated!

Have a great day!
