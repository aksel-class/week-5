// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: [
    [
      "@pinia/nuxt",
      {
        autoImports: ["defineStore", "storeToRefs"],
      },
    ],
  ],
  imports: {
    // Auto-import pinia stores defined in `~/stores`
    dirs: ["stores"],
  },
});
