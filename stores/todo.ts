interface IActivity {
  name: string;
  done: boolean;
}

export const useTodoStore = defineStore("todo", () => {
  const activityList = reactive([] as IActivity[]);

  const addActivity = (activity: IActivity) => {
    activityList.push(activity);
  };

  const isDone = (name: string) => {
    const listToUpdate = activityList.find((obj) => obj.name === name);
    if (listToUpdate) {
      const index = activityList.indexOf(listToUpdate);
      activityList[index].done = true;
    }
  };
  return { activityList, addActivity, isDone };
});
